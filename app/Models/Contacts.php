<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Laravel\Sanctum\HasApiTokens;
use App\Models\User;
use App\Models\Companys;

class Contacts extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    /**
     * paginate per_page constant value
     *
     * @var integer
     */
    public const PER_PAGE = 2; 

    // get the contacts by user ID.

    /**
     * contacts query filter by user id
     * 
     * @access public
     * @param int $id
     * 
     * @return object
     */
    public function getContactsById (int $id): object
    {
        return $this->where(['user_id' => $id])
            ->orderByDesc('id')
            ->get();
    }

    // count contacts

    /**
     * count contacts query filter by user id
     * 
     * @access public
     * @param int $id
     * 
     * @return integer
     */
    public function countContactsById (int $id): int
    {
        return $this->where(['user_id' => $id])->count();
    }

    // get the contacts paginate by user ID.

    /**
     * contacts query paginate filter by user id
     * 
     * @access public
     * @param int $id
     * 
     * @return object
     */
    public function getContactsPaginateById (int $id): object
    {
        return $this->where(['user_id' => $id])
            ->orderByDesc('id')
            ->paginate(self::PER_PAGE);
    }

    // get the contacts by user ID, contact ID.

    /**
     * contacts query row filter by contact.id, contact.user_id
     * 
     * @access public
     * @param int $id
     * @param int $uid
     * 
     * @return object
     */
    public function getContactsByUserId (int $id, int $uid): object
    {
        return $this->where(['id' => $id, 'user_id' => $uid])
            ->orderByDesc('id')
            ->first();
    }

    // delete the contacts by contact ID.

    /**
     * contacts delete filter by contact.id
     * 
     * @access public
     * @param int $id
     * 
     * @return void
     */
    public function deleteContactsById (int $id): void 
    {
        $this->where(['id' => $id])->delete();
    }

    // contact company relationship

    /**
     * contacts query join with company filter by id, company_id all order=desc by id
     * 
     * @access public
     * @
     * 
     * @return object
     */
    public function getContactsCompanyPaginateById (int $id): object
    {
        return $this->select(['*',
            'contacts.id AS contact_id',
            'contacts.name AS contact_name',
            'companys.name AS company_name'
        ])->where(['user_id' => $id])
            ->join('companys', 'companys.id', '=', 'contacts.company_id')
            ->orderByDesc('contact_id')
            ->paginate(self::PER_PAGE);
    } 

    // company functions / component

    /**
     * company query all order=desc by id
     * 
     * @access public
     * 
     * @return object
     */
    public function getCompanyAll (): object
    {
        return Companys::query()->orderByDesc('id')->get();
    }   

    // get the company by company ID.

    /**
     * company query filter by company.id order=desc by id
     * 
     * @access public
     * @param int $id
     * 
     * @return object
     */
    public function getCompanyById (int $id): object
    {
        return Companys::where(['id' => $id])->orderByDesc('id')->get();
    }   
}
