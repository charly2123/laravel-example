<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];


    /**
     * user role constant array key=value
     *
     * @var array
     */
    public const ROLE = [
        1 => 'admin',
        2 => 'user'
    ]; 

    // get query by id / auth id

    /**
     * user query filter by user id
     * 
     * @access public
     * @param int $id
     * 
     * @return object
     */
    public function getDataById (int $id): object 
    {
        return $this->where(['id' => $id])->first();
    }

    // get role
    
    /**
     * user query filter by user id
     * 
     * @access public
     * @param int $id
     * 
     * @return integer
     */
    public function getRole (int $id): int
    {
        return $this->where(['id' => $id])->first()->role;
    }
}
