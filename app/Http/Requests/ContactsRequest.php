<?php

namespace App\Http\Requests;

use App\User;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ContactsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        //$email_unique = Auth::guard('admin')->user()->id == 1 ? 'sometimes' : 'required', 'email', Rule::unique((new User)->getTable())->ignore(Auth::guard('admin')->user()->id);

        return [
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'unique:contacts,email', 'email' ],
            'password' => ['required', 'string'],
            'company' => ['required', 'integer']
        ];
    }
}
