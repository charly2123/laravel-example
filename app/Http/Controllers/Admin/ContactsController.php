<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ContactsRequest;
use App\Exceptions\InvalidOrderException;
use App\Models\Contacts;
use App\Models\User;
use Throwable;

class ContactsController extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    /**
     * contacts index page
     * 
     * @access public
     * @param  \App\Models\Contacts $contacts
     * 
     * @return \Illuminate\View\View
     */
    public function show (Contacts $contacts) : \Illuminate\View\View
    {
        return view('admin.contacts.index', [
            'page' => 'index',
            'count' => $contacts->countContactsById(Auth::guard('admin')->user()->id),
            'query' => $contacts->getContactsCompanyPaginateById(Auth::guard('admin')->user()->id)
        ]);
    }

    /**
     * contacts new page
     * 
     * @access public
     * @param  \App\Models\Contacts $contacts
     * 
     * @return Illuminate\Http\RedirectResponse || \Illuminate\View\View
     */
    public function new (Contacts $contacts): RedirectResponse|\Illuminate\View\View
    {   
        return view('admin.contacts.new.index', [
            'page' => 'new',
            'company' => $contacts->getCompanyAll()
        ]);

    }

    /**
     * contacts store function
     * 
     * @access public
     * @param \App\Http\Requests\ContactsRequest $request
     * @param  \App\Models\Contacts $contacts
     * 
     * @return Illuminate\Http\RedirectResponse || \Illuminate\View\View
     */
    public function store (ContactsRequest $request, Contacts $contacts) 
    {
        $input = $request->validated();

        $query->name = $input['name'];
        $query->email = $input['email'];
        $query->password = $input['password'];
        $query->updated_at = $input['updated_at'];
         
        if ($query->save()) {
            return redirect()->route('admin.contacts');
        }
    }

    /**
     * contacts edit page
     * 
     * @access public
     * @param int $id
     * @param  \App\Models\Contacts $contacts
     * 
     * @return Illuminate\Http\RedirectResponse || \Illuminate\View\View
     */
    public function edit (int $id, Contacts $contacts): RedirectResponse|\Illuminate\View\View
    {
        $query = $contacts->getContactsByUserId($id, Auth::guard('admin')->user()->id);
        
        if (!$query) {
            return redirect()->route('admin.contacts');
        }
        
        return view('admin.contacts.edit', [
            'page' => 'edit',
            'id' => $id,
            'query' => $query,
            'company' => $contacts->getCompanyById($query->company_id),
        ]);
    }

    /**
     * contacts update function
     * 
     * @access public
     * @param int $id
     * @param \App\Models\Contacts $contacts
     * @param \App\Http\Requests\ContactsRequest $request
     * 
     * @return Illuminate\Http\RedirectResponse || \Illuminate\View\View
     */
    public function update (int $id, Contacts $contacts, ContactsRequest $request): RedirectResponse|\Illuminate\View\View
    {
        $query = $contacts->getContactsByUserId($id, Auth::guard('admin')->user()->id);

        if (!$query) {
            return redirect()->route('admin.contacts');
        }

        $input = $request->only(['name', 'email', 'password', 'updated_at']);

        $query->name = $input['name'];
        $query->email = $input['email'];
        $query->password = $input['password'];
        $query->updated_at = $input['updated_at'];
         
        if ($query->save()) {
            return redirect()->route('admin.contacts.edit', ['id' => $query->id]);
        }
    }

    /**
     * contacts delete page
     * 
     * @access public
     * @param int $id
     * @param  \App\Models\Contacts $contacts
     * 
     * @return \Illuminate\View\View
     */
    public function delete (int $id, Contacts $contacts) : \Illuminate\View\View
    {
        $this->deleteContactsById($id);

        return view('admin.contacts.delete', [
            'page' => 'edit',
            'id' => $id,
            'query' => $contacts->getContactsByUserId($id, Auth::guard('admin')->user()->id)
        ]);
    }
}
