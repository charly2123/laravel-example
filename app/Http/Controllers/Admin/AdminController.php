<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UsersRequest;
use App\Models\User;

class AdminController extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    /**
     * admin index page
     * 
     * @access public
     * 
     * @return \Illuminate\View\View
     */
    public function show (User $user) : \Illuminate\View\View
    {
        return view('admin.index', [
            'page' => 'index',
            'role' => $user->getRole(Auth::guard('admin')->user()->id)
        ]);
    }

    /**
     * admin profile page
     * 
     * @access public
     * 
     * @return \Illuminate\View\View
     */
    public function profile (User $user) : \Illuminate\View\View
    {
        return view('admin.profile.index', [
            'page' => 'profile',
            'query' => $user->getDataById(Auth::guard('admin')->user()->id),
            'role' => $user->getRole(Auth::guard('admin')->user()->id),
            'all_role' => $user::ROLE
        ]);
    }

    /**
     * admin profile update function
     * 
     * @access public
     * 
     * @return \Illuminate\View\View
     */
    public function update (User $user, UsersRequest $request) : \Illuminate\View\View
    {
        $input = $request->validated();
        $query = $user->getDataById(Auth::guard('admin')->user()->id);

        $query->name = $input['name'];
        $query->email = $input['email'];
        $query->password = $input['password'];
        $query->role = $input['role'];
         
        if ($query->save()) {
            return redirect()->route('admin.profile');
        }
    }

    /**
     * admin authentication.
     * 
     * @access public
     * @param Illuminate\Http\Request $request
     * 
     * @return Illuminate\Http\RedirectResponse
     */
    public function authenticate(Request $request): RedirectResponse
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::guard('admin')->attempt($credentials)) {
            $request->session()->regenerate();
        }

        return redirect()->route('admin.dashboard');
    }

    /**
     * admin login page
     * 
     * @access public
     * 
     * @return \Illuminate\View\View
     */
    public function login () : \Illuminate\View\View
    {
        return view('admin.login', [
            'page' => 'login'
        ]);
    }

    /**
     * admin register page
     * 
     * @access public
     * 
     * @return \Illuminate\View\View
     */
    public function register () : \Illuminate\View\View
    {
        return view('admin.register', [
            'page' => 'register'
        ]);
    }

    /**
     * log the admin out of the application.
     * 
     * @access public
     * @param Illuminate\Http\Request $request
     * 
     * @return Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request): RedirectResponse
    {
        Auth::guard('admin')->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();
     
        return redirect()->route('admin.login');
    }
}
