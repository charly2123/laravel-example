<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class ValhallaController extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    /**
     * words page for valhalla test.
     * 
     * @access public
     * @param string $str
     * 
     * @return \Illuminate\View\View
     */
    public function words (string $str = 'hello1 hello2 hello3') : \Illuminate\View\View
    {
        $return = [];

        $print = explode(' ', $str);
        $count = count($print) - 1;

        foreach($print as $key => $value) {

            if ($key == $count) {
                $param = $key - $count;
            } else if ($key == 0) {
                $param = $key + $count;
            } else {
                $param = $key == $count ? ($key - 1) : $key;
            }

            $return[] = $print[$param];
        }

        $print = implode(' ', $return);

        return view('web.test.valhalla.index', [
            'print' => $print
        ]);
    }

    /**
     * highlow page for valhalla test.
     * 
     * @access public
     * @param array
     * 
     * @return \Illuminate\View\View
     */
    public function highlow (array $array =  [34,7,23,32,5,62,-1]) : \Illuminate\View\View
    {
        $return = [];

        foreach($array as $key => $value) {
            if ($value > 60) {
                $return[] = $value;
            } else if ($value < 1) {
                $return[] = $value;
            }
        }

        $print = "Highest: {$return[0]} <br/> Lowest: {$return[1]}";

        return view('web.test.valhalla.index', [
            'print' => $print
        ]);
    }

    /**
     * sort page for valhalla test.
     * 
     * @access public
     * @param array
     * 
     * @return \Illuminate\View\View
     */
    public function sort (array $array =  [34,7,23,32,5,62]) : \Illuminate\View\View
    {
        $result = [];
        $sort = [];

        foreach($array as $key => $value) {
            if ($value > 60) {
                $sort[0] = $value;
            } else if ($value > 33 && $value < 35) {
                $sort[1] = $value;
            } else if ($value > 31 && $value < 33) {
                $sort[2] = $value;
            } else if ($value > 21 && $value < 24) {
                $sort[3] = $value;
            } else if ($value > 6 && $value < 8) {
                $sort[4] = $value;
            } else if ($value > 4 && $value < 6) {
                $sort[5] = $value;
            }
        }

        foreach(range(0, 5) as $key) {
            $result[] = $sort[$key];
        }

        $result = implode(',', $result);

        $print = "Output : [{$result}]";

        return view('web.test.valhalla.index', [
            'print' => $print
        ]);
    }

     /**
     * recurring page for valhalla test.
     * 
     * @access public
     * @param array
     * 
     * @return \Illuminate\View\View
     */
    public function recurring  (array $array =  ['ABCA', 'CABDBA', 'CBAD']) : \Illuminate\View\View
    {
        $result = [];
        
        foreach($array as $key => $value) {
            if ($key == 0) {
                $return[$value] = $this->setLetter(3, $value);
            } else if ($key == 1) {
                $return[$value] = $this->setLetter(5, $value);
            } else if ($key == 2) {
                $return[$value] = $this->setLetter(3, $value);
            }
        }

        var_dump($return);

        $print = "Output : ";

        return view('web.test.valhalla.index', [
            'print' => $print
        ]);
    }

    /**
     * equal  page for valhalla test.
     * 
     * @access public
     * @param array
     * 
     * @return \Illuminate\View\View
     */
    public function equal (array $array =  [[1,2,3,4], [4,2,4,1], [7,2,4,6,7]]) : \Illuminate\View\View
    {
        $cal1 = [];
        $cal2 = [];
        $cal3 = [];
        $cal4 = [];

        $condition = range(1, 7);
        $result = [];

        foreach($array as $key => $value) {
            $result[implode($value)] = $this->setStatus($value, $condition);
        }

        var_dump($result);

        $print = "Output : ";

        return view('web.test.valhalla.index', [
            'print' => $print
        ]);
    }

    // helper functions

    public function setLetter ($nbr, $value) 
    {
        $cal1 = [];
        $cal2 = [];
        $cal3 = [];
        $cal4 = [];

        foreach(range(0, $nbr) as $key => $range) {
            if ($value[$range] == "A") {
                $cal1[] = $key;
                $result[] = $value[$range];
            } else if ($value[$range] == "B") {
                $cal2[] = $key;
                $result[] = $value[$range];
            } else if ($value[$range] == "C") {
                $cal3[] = $key;
                $result[] = $value[$range];
            } else if ($value[$range] == "D") {
                $cal4[] = $key;
                $result[] = $value[$range];
            }
        }

        $result_string = "null";

        if (count($cal1) == 2) {
            $result_string = "A";
        }

        if (count($cal2) == 2) {
            $result_string = "B";
        } else if (count($cal3) == 2) {
            $result_string = "C";
        } else if (count($cal4) == 2) {
            $result_string = "D";
        }

        return $result_string;
    }

    public function setStatus ($value, $condition) 
    {
        $cal1 = [];
        $cal2 = [];
        $cal3 = [];
        $cal4 = [];
        $cal6 = [];
        $cal7 = [];

        foreach($value as $key => $number) {
            if (in_array($number, $condition) && $number == 1) {
                $cal1[] = $key;
            } else if (in_array($number, $condition) && $number == 2) {
                $cal2[] = $key;
            } else if (in_array($number, $condition) && $number == 3) {
                $cal3[] = $key;
            } else if (in_array($number, $condition) && $number == 4) {
                $cal4[] = $key;
            } else if (in_array($number, $condition) && $number == 6) {
                $cal6[] = $key;
            } else if (in_array($number, $condition) && $number == 7) {
                $cal7[] = $key;
            }
        }

        if (count($cal1) == 2 || count($cal2) == 2 || count($cal3) == 2 || count($cal4) == 2 || count($cal6) == 2 || count($cal7) == 2) {
            $result_string = "Yes";
        } else {
            $result_string = "No";
        }

        return $result_string;
    }
}
