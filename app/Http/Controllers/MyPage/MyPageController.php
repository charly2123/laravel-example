<?php

namespace App\Http\Controllers\MyPage;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class MyPageController extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    /**
     * mypage index page
     * 
     * @access public
     * 
     * @return \Illuminate\View\View
     */
    public function show () : \Illuminate\View\View
    {
        return view('mypage.index', [
            'page' => 'index'
        ]);
    }

    /**
     * admin authentication.
     * 
     * @access public
     * 
     * @return Illuminate\Http\RedirectResponse
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::guard('mypage')->attempt($credentials)) {
            $request->session()->regenerate();
            return redirect()->intended('mypage.index');
        }
    }

    /**
     * mypage login page
     * 
     * @access public
     * 
     * @return \Illuminate\View\View
     */
    public function login () : \Illuminate\View\View
    {
        return view('mypage.login', [
            'page' => 'login'
        ]);
    }

    /**
     * log the mypage out of the application.
     * 
     * @access public
     * @param Illuminate\Http\Request $request
     * 
     * @return Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request): RedirectResponse
    {
        Auth::guard('mypage')->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();
     
        return redirect()->route('mypage.login');
    }
}
