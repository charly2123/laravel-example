<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AdminController extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    /**
     * admin index page
     * 
     * @access public
     * 
     * @return \Illuminate\View\View
     */
    public function show (User $user) : \Illuminate\View\View
    {
        $role = $user->getRole('charly@example.org');

        return view('admin.index', [
            'page' => 'index'
        ]);
    }

    /**
     * admin authentication.
     * 
     * @access public
     * @param Illuminate\Http\Request $request
     * 
     * @return Illuminate\Http\RedirectResponse
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::guard('admin')->attempt($credentials)) {
            $request->session()->regenerate();
            return redirect()->intended('admin.dashboard');
        }
    }

    /**
     * admin login page
     * 
     * @access public
     * 
     * @return \Illuminate\View\View
     */
    public function login () : \Illuminate\View\View
    {
        return view('admin.login', [
            'page' => 'login'
        ]);
    }

    /**
     * log the admin out of the application.
     * 
     * @access public
     * @param Illuminate\Http\Request $request
     * 
     * @return Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request): RedirectResponse
    {
        Auth::guard('admin')->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();
     
        return redirect()->route('admin.login');
    }
}
