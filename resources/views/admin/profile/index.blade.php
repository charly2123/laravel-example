<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel > Admin</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
      
        <style>
            .login-header {
                border: 1px solid #dedede;
                padding: 30px;
                margin-top: 20px;
            }
            form#login-form {
                border: 1px solid #dedede;
                padding: 30px;
                margin-top: 20px;
            }
        </style>

    </head>
    <body class="antialiased">

        @include('template.admin.header')
        @include('template.admin.menu')

        <div class="container">
            <div class="dashboard-header">
                <h1>
                    Dashboard > Profile
                </h1>
            </div>
            <div class="dashboard-content">

                <form method="POST" action="{{ route('admin.profile.update') }}" id="login-form">
                    @csrf

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input name="name" type="text" value="{{ old('name') ?? $query->name ?? '' }}" class="form-control mb-2" id="inlineFormInput" placeholder="Name">
                        </div>
                        <div class="form-group col-md-6">
                            <input name="email" type="email" value="{{ old('email') ?? $query->email ?? '' }}" class="form-control mb-2" id="inputEmail4" placeholder="Email">
                        </div>
                        <div class="form-group col-md-6">
                            <input name="password" type="password" value="{{ old('password') ?? $query->password ?? '' }}" class="form-control mb-2" id="inputPassword4" placeholder="Password" disabled>
                        </div>
                        <div class="form-group col-md-6">
                            <select name="role" class="form-select mb-2" aria-label="select role">
                                @foreach($all_role as $key => $value)
                                    @php
                                        $selected = $query->role == $key ?? 'selected';
                                    @endphp
                                    <option value="{{ $key }}" {{ $selected }}>{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                    
                </form>

            </div>
        </div>

        @include('template.admin.footer')

    </body>
</html>
