<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel > Admin</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">

        <style>
                .login-header {
                    border: 1px solid #dedede;
                    padding: 30px;
                    margin-top: 20px;
                }
                form#login-form {
                    border: 1px solid #dedede;
                    padding: 30px;
                    margin-top: 20px;
                }
        </style>

    </head>
    <body class="antialiased">

        @include('template.admin.header')
        @include('template.admin.menu')
        
        <div class="container">
            <div class="dashboard-header">
                <h1>
                    Dashboard Welcome, Admin
                </h1>
            </div>
            <div class="dashboard-header">
                
            </div>
        </div>

        @include('template.admin.footer')

    </body>
</html>
