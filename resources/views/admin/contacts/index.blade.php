<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel > Admin</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
      
        <style>
            .login-header {
                border: 1px solid #dedede;
                padding: 30px;
                margin-top: 20px;
            }
            form#login-form {
                border: 1px solid #dedede;
                padding: 30px;
                margin-top: 20px;
            }
        </style>

    </head>
    <body class="antialiased">

        @include('template.admin.header')
        @include('template.admin.menu')

        <div class="container">
            <div class="dashboard-header">
                <h1>
                    Dashboard > Contacts [{{ $count }}]
                </h1>
            </div>
            <div class="dashboard-content">

                <!-- Search form -->
                <div class="search-form md-form mt-0">
                    <div class="row">
                        <div class="col-12 col-sm-8 col-md-8">
                            <a href="{{ route('admin.contacts.new') }}">Add Contacts</a>
                        </div>
                        <div class="col-12 col-sm-4 col-md-4">
                            <input class="search-input form-control" type="text" placeholder="Search" aria-label="Search" />
                        </div>
                    </div>
                </div>

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Company</th>
                            <th scope="col">Email</th>
                            <th scope="col">Handle</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($query as $key => $value)
                            <tr>
                                <th scope="row">{{ $value->contact_id }}</th>
                                <td>{{ $value->contact_name }}</td>
                                <td>{{ $value->company_name }}</td>
                                <td>{{ $value->email }}</td>
                                <td>
                                    <a href="{{ route('admin.contacts.edit', [
                                        'id' => $value->contact_id
                                    ]) }}">Edit</a> |
                                    <a href="{{ route('admin.contacts.delete', [
                                        'id' => $value->contact_id
                                    ]) }}">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                
                <!-- passing additional data to the view... -->
                {{ $query->onEachSide(1)->links('vendor.pagination.bootstrap-5', ['id' => 1]) }}

            </div>
        </div>

        @include('template.admin.footer')

        <script>
            $('input.search-input').keyup(function(e) {
                var $value = $(this).val();
                console.log($value);
            });
        </script>

    </body>
</html>
