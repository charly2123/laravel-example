  
<menu class="container">
    <div class="menu-wrap">
        <div class="row">
            <div class="col-12 col-sm-8 col-md-8">
                <a href="{{ route('admin.dashboard') }}">Home</a> |
                <a href="{{ route('admin.contacts') }}">Contacts</a>
            </div>
            <div class="col-12 col-sm-4 col-md-4">
                <a href="{{ route('admin.profile') }}" class="btn btn-success">Profile</a>
                <a href="{{ route('admin.logout') }}" class="btn btn-danger">Logout</a>
            </div>
        </div>
    </div>
</menu>
