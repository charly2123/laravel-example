<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\ContactsController;

/*
|--------------------------------------------------------------------------
| Admin Routes /admin/{slug}
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// auth
Route::middleware(['auth:admin', 'auth.session'])->group(function () {

    // dashboard
    Route::controller(AdminController::class)->group(function () {
        Route::get('/', 'show')->name('dashboard');
        Route::get('/logout', 'logout')->name('logout');
        Route::get('/profile', 'profile')->name('profile');
    });

    // contacts
    Route::controller(ContactsController::class)->group(function () {
        Route::get('/contacts', 'show')->name('contacts');
        Route::get('/contacts/new', 'new')->name('contacts.new');
        Route::get('/contacts/edit/{id}', 'edit')->name('contacts.edit');
        Route::get('/contacts/delete/{id}', 'delete')->name('contacts.delete');
        Route::post('/contacts/store', 'store')->name('contacts.store');
        Route::post('/contacts/edit/{id}', 'update')->name('contacts.update');
    });
});

// guest
Route::middleware(['guest:admin'])->group(function () {

    // form
    Route::controller(AdminController::class)->group(function () {
        Route::get('/login', 'login')->name('login');
        Route::get('/register', 'login')->name('register');
        Route::post('/authenticate', 'authenticate')->name('authenticate');
    });
});

