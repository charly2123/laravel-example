<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\MyPage\MyPageController;

//use App\Http\Controllers\Admin\ProfileController;
//use App\Http\Controllers\Admin\ContactsController;

/*
|--------------------------------------------------------------------------
| Admin Routes /admin/{slug}
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware(['auth.mypage'])->group(function () {

    Route::controller(MyPageController::class)->group(function () {
        // dashboard
        Route::get('/', 'show')->name('index');
    });
    
    // profile
    // contacts
});

Route::middleware(['guest:mypage'])->group(function () {
    Route::controller(MyPageController::class)->group(function () {
        // form
        Route::get('/login', 'login')->name('login');
        Route::post('/authenticate', 'authenticate')->name('authenticate');
    });
});
